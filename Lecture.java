/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package attendanceregistration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Lecture {
	
	private int lNumber;
	private String teacherId;
	private String courseId;
	private String time;
        private String date;
	List<Student> students = new ArrayList();
        
        public List getStudents(){
            return students;
        }
	
	public Lecture() {
		// TODO Auto-generated constructor stub
	}
	
	public Lecture(int lNumber, String teacherId, String courseId, String time, String Date) {
		this.lNumber=lNumber;
		this.teacherId=teacherId;
		this.courseId=courseId;
		this.time=time;
                this.date = Date;
	}


	public int getlNumber() {
		return lNumber;
	}


	public void setlNumber(int lNumber) {
		this.lNumber = lNumber;
	}


	public String getTeacherId() {
		return teacherId;
	}


	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}


	public String getCourseId() {
		return courseId;
	}


	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}


	public String getTime() {
		return time;
	}


	public void setTime(String time) {
		this.time = time;
	}
        
        public String getsDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

}
