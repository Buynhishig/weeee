/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package attendanceregistration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminar {

	private int sNumber;
	private String teacherId;
	private String courseId;
	private String time;
        private String date;
        List<Student> students = new ArrayList();
        public List<Student> getStudents(){
            return students;
        } 
	public Seminar() {
		// TODO Auto-generated constructor stub
	}
	
	public Seminar(int sNumber, String teacherId, String courseId, String time, String date) {
		this.sNumber=sNumber;
		this.teacherId=teacherId;
		this.courseId=courseId;
		this.time=time;
                this.date = date;
	}
        public String getsDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getsNumber() {
		return sNumber;
	}
	public void setsNumber(int sNumber) {
		this.sNumber = sNumber;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	

}
