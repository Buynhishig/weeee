/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package attendanceregistration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Course {

	private String courseId;
	private String cName;
	private int lNumber;
	private int sNumber;
	
	public Course(String courseId, String cName, int lNumber, int sNumber) {
		this.courseId=courseId;
		this.cName=cName;
		this.lNumber=lNumber;
		this.sNumber=sNumber;
		
	}
        List<Lecture> lecture = new ArrayList();
        List<Seminar> seminar = new ArrayList();
        List<Teacher> teachers = new ArrayList();
        List<Student> students = new ArrayList();
        public List<Lecture> getLect(){
            return lecture;
        }
        public List<Seminar> getSeminar(){
            return seminar;
        }
	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public int getlNumber() {
		return lNumber;
	}

	public void setlNumber(int lNumber) {
		this.lNumber = lNumber;
	}

	public int getsNumber() {
		return sNumber;
	}

	public void setsNumber(int sNumber) {
		this.sNumber = sNumber;
	}
	
	

}

