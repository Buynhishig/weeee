/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package attendanceregistration;

import java.util.ArrayList;
import java.util.List;

public class Teacher {

	private String tId;
	private String password;
	private String courseId;
	private String lName;
	private String fName;
	private String degree;
        List<Course> courses = new ArrayList<>();
	public Teacher() {
		// TODO Auto-generated constructor stub
	}
	
	public Teacher(String tId, String password, String courseId, String lName, String fName, String degree) {
		this.tId=tId;
		this.password=password;
		this.courseId=courseId;
		this.lName=lName;
		this.fName=fName;
		this.degree=degree;
		
	}
        
        public List getCourses(){
            return courses;
        }

	public String getId() {
		return tId;
	}

	public void settId(String tId) {
		this.tId = tId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}
	
	
}
