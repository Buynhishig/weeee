/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package attendanceregistration;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class AttendanceRegistration {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner sc = new Scanner(System.in);
        List<Teacher> teachers = new ArrayList();
        List<Course> courses = new ArrayList();

        Course SoftwareDevelopment = new Course("01", "SoftwareDevelopment", 16, 16);
        Course math = new Course("02", "math", 16, 16);
        Course SoftwareStruct = new Course("03", "SoftStruct", 16, 16);
        Course Algorithm = new Course("04", "Algorithm", 16, 16);

        courses.add(SoftwareDevelopment);
        courses.add(math);
        courses.add(SoftwareStruct);
        courses.add(Algorithm);

        Teacher t1 = new Teacher("100", "1111", "01", "Bat", "Bayrmaa", "Master");
        Teacher t2 = new Teacher("101", "2222", "02", "Dorj", "Tsetseg", "Master");
        Teacher t3 = new Teacher("102", "3333", "04", "Gombo", "Gerelzul", "Bachelor");
        teachers.add(t1);
        teachers.add(t2);
        teachers.add(t3);

        t1.courses.add(SoftwareDevelopment);
        t2.courses.add(SoftwareStruct);
        t2.courses.add(math);
        t2.courses.add(SoftwareDevelopment);
        t3.courses.add(Algorithm);

        for (int i = 0; i < 16; i++) {
            Algorithm.lecture.add(new Lecture(i, "102", "11", "12:30", "2018.11.11"));
            Algorithm.seminar.add(new Seminar(i, "102", "11", "12:30", "2018.11.13"));
        }

        for (int i = 0; i < 16; i++) {
            SoftwareDevelopment.lecture.add(new Lecture(i, "12", "11", "12:30", "2018.11.11"));
            SoftwareDevelopment.seminar.add(new Seminar(i, "12", "09", "14:20", "2018.11.11"));
        }

        for (int i = 0; i < 16; i++) {
            SoftwareStruct.lecture.add(new Lecture(2, "12", "11", "13:30", "2018.11.11"));
            SoftwareStruct.seminar.add(new Seminar(2, "12", "11", "13:30", "2018.11.11"));
            math.lecture.add(new Lecture(1, "12", "11", "13:30", "2018.11.11"));
            math.seminar.add(new Seminar(1, "12", "11", "13:30", "2018.11.11"));
        }

        Student student1 = new Student("00001", "Jhon", "Maria", "01");
        Student student2 = new Student("00002", "Jhon", "Azaa", "01");
        Student student3 = new Student("00003", "Jhon", "Khajidma", "01");
        Student student4 = new Student("00004", "Jhon", "Oyunbileg", "01");
        Student student5 = new Student("00005", "Jhon", "Buynkhishig", "01");

        Student student11 = new Student("00001", "Jhon", "Maria", "02");
        Student student12 = new Student("00002", "Jhon", "Azaa", "02");
        Student student13 = new Student("00003", "Jhon", "Khajidma", "02");
        Student student14 = new Student("00004", "Jhon", "Oyunbileg", "02");
        Student student15 = new Student("00005", "Jhon", "Buynkhishig", "02");

        Student student21 = new Student("00001", "Jhon", "Bayr", "03");
        Student student22 = new Student("00002", "Jhon", "BOld", "03");
        Student student23 = new Student("00003", "Jhon", "Tuya", "03");
        Student student24 = new Student("00004", "Jhon", "Oyunbileg", "03");
        Student student25 = new Student("00005", "Jhon", "Buynkhishig", "03");

        math.students.add(student11);
        math.students.add(student12);
        math.students.add(student13);
        math.students.add(student14);
        math.students.add(student15);

        SoftwareDevelopment.students.add(student1);
        SoftwareDevelopment.students.add(student2);
        SoftwareDevelopment.students.add(student3);
        SoftwareDevelopment.students.add(student4);
        SoftwareDevelopment.students.add(student5);
        List<Student> sDStudents = new ArrayList();
        sDStudents.add(student1);
        sDStudents.add(student2);
        sDStudents.add(student3);
        sDStudents.add(student4);
        sDStudents.add(student5);
        for (int i = 0; i < 16; i++) {
            SoftwareDevelopment.getLect().get(i).students.addAll(sDStudents);
            SoftwareDevelopment.getSeminar().get(i).students.addAll(sDStudents);
        }

        SoftwareStruct.students.add(student21);
        SoftwareStruct.students.add(student22);
        SoftwareStruct.students.add(student23);
        SoftwareStruct.students.add(student24);
        SoftwareStruct.students.add(student25);

        for (int i = 0; i < math.students.size(); i++) {
            for (int j = 0; j < math.lecture.size(); j++) {
                math.getLect().get(j).students.add(math.students.get(i));
            }
            for (int j = 0; j < math.seminar.size(); j++) {
                math.getLect().get(j).students.add(math.students.get(i));
            }
        }

        Student student31 = new Student("00001", "Jhon", "Maria", "04");
        Student student32 = new Student("00002", "Jhon", "Azaa", "04");
        Student student33 = new Student("00003", "Jhon", "Khajidma", "04");
        Student student34 = new Student("00004", "Jhon", "Oyunbileg", "04");
        Student student35 = new Student("00005", "Jhon", "Buynkhishig", "04");

        Algorithm.students.add(student31);
        Algorithm.students.add(student32);
        Algorithm.students.add(student33);
        Algorithm.students.add(student34);
        Algorithm.students.add(student35);

        List<Student> algoStudents = new ArrayList();
        algoStudents.add(student31);
        algoStudents.add(student32);
        algoStudents.add(student33);
        algoStudents.add(student34);
        algoStudents.add(student35);
        for (int i = 0; i < 16; i++) {
            Algorithm.getLect().get(i).students.addAll(algoStudents);
            Algorithm.getSeminar().get(i).students.addAll(algoStudents);
        }

        System.out.println("Enter id:");
        String id = sc.next();
        System.out.println("Enter password:");
        int password = sc.nextInt();
        for (int i = 0; i < teachers.size(); i++) {
            if (id.equals(teachers.get(i).getId())) {
                Teacher teacher = teachers.get(i);
                System.out.println("tentsuu");
                List<Course> teachersCourses = teacher.getCourses();
                System.out.println("Tanii hicheeluud:");
                for (int p = 0; p < teachersCourses.size(); p++) {
                    System.out.println(teachersCourses.get(p).getcName());
                }
                System.out.println("Burtgeh hicheelee songono uu?");
                System.out.println("0 SoftDev , 1 math ,2 Algorithm");
                int ch = sc.nextInt();
                if (ch == 0) {

                    for (int k = 0; k < teachersCourses.size(); k++) {
                        Course chosenCourse = teachersCourses.get(k);
                        System.out.println(chosenCourse.getcName());
                        if (chosenCourse.getcName().equals("SoftwareDevelopment")) {
                            System.out.println("tentsuu");
//                            for (int j = 0; j < courses.size(); j++) {
                            System.out.println("Lekts uu semnar uu?");
                            System.out.println("Lekts 1 ; semnar 2");
                            int choice = sc.nextInt();
                            if (choice == 1) {
                                int lectNum;
                                System.out.println("Lektsiin dugaaraa oruulna uu?");
                                lectNum = sc.nextInt();
                                for (int q = 0; q < 1; q++) {
                                    Lecture l = chosenCourse.getLect().get(lectNum);
                                    System.out.println("newterlee");
                                    System.out.println(l.getlNumber());
                                    if (l.getlNumber() == lectNum) {
                                        System.out.println("amjilttai");
                                        List<Student> s = l.getStudents();
                                        for (int t = 0; t < chosenCourse.students.size(); t++) {
                                            System.out.println(s.get(t).getfName());
                                            Attendance at = new Attendance();
                                            s.get(t).attendances.add(at);
                                            s.get(t).attendances.get(i).setState();
                                        }
                                        System.out.println("Hadgalah medeelliig haruulna uu!");
                                        for (int t = 0; t < chosenCourse.students.size(); t++) {
                                            System.out.println(s.get(t).getfName());
                                            System.out.println(s.get(t).attendances.get(0).getState());
                                        }
                                    }
                                }
//                                   

                            } else if (choice == 2) {
                                int semNum;
                                System.out.println("Semnariin dugaaraa oruulna uu?");
                                semNum = sc.nextInt();
                                chosenCourse.getSeminar().get(semNum);
                                for (int q = 0; q < 1; q++) {
                                    Seminar l = chosenCourse.getSeminar().get(semNum);
                                    System.out.println("newterlee");
                                    System.out.println(l.getsNumber());
                                    if (l.getsNumber() == semNum) {
                                        System.out.println("amjilttai");
                                        List<Student> s = l.getStudents();
                                        for (int t = 0; t < chosenCourse.students.size(); t++) {
                                            System.out.println(s.get(t).getfName());
                                            Attendance at = new Attendance();
                                            s.get(t).attendances.add(at);
                                            s.get(t).attendances.get(i).setState();
                                        }
                                        System.out.println("Hadgalah medeelliig haruulna uu!");
                                        for (int t = 0; t < chosenCourse.students.size(); t++) {
                                            System.out.println(s.get(t).getfName());
                                            System.out.println(s.get(t).attendances.get(0).getState());
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (ch == 1) {
                    for (int k = 0; k < teachersCourses.size(); k++) {
                        Course chosenCourse = teachersCourses.get(k);
                        System.out.println(chosenCourse.getcName());
                        if (chosenCourse.getcName().equals("math")) {
                            System.out.println("tentsuu");
                            for (int j = 0; j < courses.size(); j++) {
                                System.out.println("Lekts uu semnar uu?");
                                System.out.println("Lekts 1 ; semnar 2");
                                int choice = sc.nextInt();
                                if (choice == 1) {
                                    int lectNum;
                                    System.out.println("Lectsiin dugaaraa oruulna uu?");
                                    lectNum = sc.nextInt();
//                                    Lecture l = chosenCourse.getLect().get(lectNum);
//                                    l.getStudents();
                                    for (int q = 0; q < 1; q++) {
                                        Lecture l = chosenCourse.getLect().get(lectNum);
                                        System.out.println("newterlee");
                                        System.out.println(l.getlNumber());
                                        if (l.getlNumber() == lectNum) {
                                            System.out.println("amjilttai");
                                            List<Student> s = l.getStudents();
                                            for (int t = 0; t < chosenCourse.students.size(); t++) {
                                                System.out.println(s.get(t).getfName());
                                                Attendance at = new Attendance();
                                                s.get(t).attendances.add(at);
                                                s.get(t).attendances.get(i).setState();
                                            }
                                            System.out.println("Hadgalah medeelliig haruulna uu!");
                                            for (int t = 0; t < chosenCourse.students.size(); t++) {
                                                System.out.println(s.get(t).getfName());
                                                System.out.println(s.get(t).attendances.get(0).getState());
                                            }
                                        }
                                    }

                                } else if (choice == 2) {
                                    int semNum;
                                    System.out.println("Semnariin dugaaraa oruulna uu?");
                                    semNum = sc.nextInt();
//                                    Seminar sem = chosenCourse.getSeminar().get(semNum);
//                                   sem.getStudents();
                                    for (int q = 0; q < 1; q++) {
                                        Seminar l = chosenCourse.getSeminar().get(semNum);
                                        System.out.println("newterlee");
                                        System.out.println(l.getsNumber());
                                        if (l.getsNumber() == semNum) {
                                            System.out.println("amjilttai");
                                            List<Student> s = l.getStudents();
                                            for (int t = 0; t < chosenCourse.students.size(); t++) {
                                                System.out.println(s.get(t).getfName());
                                                Attendance at = new Attendance();
                                                s.get(t).attendances.add(at);
                                                s.get(t).attendances.get(i).setState();
                                            }
                                            System.out.println("Hadgalah medeelliig haruulna uu!");
                                            for (int t = 0; t < chosenCourse.students.size(); t++) {
                                                System.out.println(s.get(t).getfName());
                                                System.out.println(s.get(t).attendances.get(0).getState());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                } else if (ch == 2) {
                    for (int k = 0; k < teachersCourses.size(); k++) {
                        Course chosenCourse = teachersCourses.get(k);

                        System.out.println(chosenCourse.getcName());
                        if (chosenCourse.getcName().equals("Algorithm")) {
                            System.out.println("tentsuu");
                            for (int j = 0; j < courses.size(); j++) {
                                System.out.println("Lekts uu semnar uu?");
                                System.out.println("Lekts 1 ; semnar 2");
                                int choice = sc.nextInt();
                                if (choice == 1) {
                                    int lectNum;
                                    System.out.println("Lectsiin dugaaraa oruulna uu?");
                                    lectNum = sc.nextInt();
                                    for (int q = 0; q < 1; q++) {
                                        Lecture l2 = chosenCourse.getLect().get(lectNum);
                                        System.out.println("newterlee");
                                        System.out.println(l2.getlNumber());
                                        if (l2.getlNumber() == lectNum) {
                                            System.out.println("amjilttai");
                                            List<Student> s = l2.getStudents();
                                            for (int t = 0; t < chosenCourse.students.size(); t++) {
                                                System.out.println(s.get(t).getfName());
                                                Attendance at = new Attendance();
                                                s.get(t).attendances.add(at);
                                                s.get(t).attendances.get(0).setState();
                                            }
                                            System.out.println("Hadgalah medeelliig haruulna uu!");
                                            for (int t = 0; t < chosenCourse.students.size(); t++) {
                                                System.out.println(s.get(t).getfName());
                                                System.out.println(s.get(t).attendances.get(0).getState());
                                            }
                                        }
                                    }

                                } else if (choice == 2) {
                                    int semNum;
                                    System.out.println("Semnariin dugaaraa oruulna uu?");
                                    semNum = sc.nextInt();
//                                    Seminar sem = chosenCourse.getSeminar().get(semNum);
//                                    sem.getStudents();
                                    for (int q = 0; q < 1; q++) {
                                        Seminar l = chosenCourse.getSeminar().get(semNum);
                                        System.out.println("newterlee");
                                        System.out.println(l.getsNumber());
                                        if (l.getsNumber() == semNum) {
                                            System.out.println("amjilttai");
                                            List<Student> s = l.getStudents();
                                            for (int t = 0; t < chosenCourse.students.size(); t++) {
                                                System.out.println(s.get(t).getfName());
                                                Attendance at = new Attendance();
                                                s.get(t).attendances.add(at);
                                                s.get(t).attendances.get(0).setState();
                                            }
                                            System.out.println("Hadgalah medeelliig haruulna uu!");
                                            for (int t = 0; t < chosenCourse.students.size(); t++) {
                                                System.out.println(s.get(t).getfName());
                                                System.out.println(s.get(t).attendances.get(0).getState());
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                } else {
                    System.out.println("Dahin oroldoj uznuu tanii oroh hicheel buruu baina");
                }
            }
        }

    }
}
