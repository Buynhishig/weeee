/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package attendanceregistration;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class Student {
    String sID;
    String lName;
    String fName;
    String courseID;
    List<Attendance> attendances = new ArrayList();
    public Student( String sID ,String lName ,String fName , String courseID){
        this.sID = sID ;
        this.lName = lName;
        this.fName = fName;
       this.courseID = courseID;
    }
       public String getCourseID(){
        return courseID;
    }
    public void setCourseID(String courseID){
        this.courseID = courseID;
    }
    public List getAttendances(){
        return attendances;
    }
    
    public String getsID() {
        return sID;
    }

    public void setsID(String sID) {
        this.sID = sID;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }
}
